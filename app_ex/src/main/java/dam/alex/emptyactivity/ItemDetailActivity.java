package dam.alex.emptyactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.service.autofill.OnClickAction;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import dam.alex.emptyactivity.module.Item;

public class ItemDetailActivity extends AppCompatActivity implements View.OnClickListener {
    Item nuevoItem;
    ImageView imageView;
    TextView api;
    TextView nombre;
    TextView version;
    TextView fecha;
    private ActionBar bar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        nuevoItem=(Item) getIntent().getSerializableExtra("info");
        super.onCreate(savedInstanceState);
        bar=getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_item_detail);
        anyadirInformacion();

    }
    private void anyadirInformacion(){
        this.setTitle(nuevoItem.getNombre());
        this.imageView=findViewById(R.id.imagen);
        this.fecha=findViewById(R.id.fecha);
        this.api=findViewById(R.id.api);
        this.version=findViewById(R.id.version);
        this.nombre=findViewById(R.id.nombre);

        this.imageView.setImageResource(nuevoItem.getIdImagen());
        this.fecha.setText(nuevoItem.getAnyolanzamiento());
        this.api.setText(nuevoItem.getVersionApi());
        this.version.setText(nuevoItem.getVersion());
        this.nombre.setText(nuevoItem.getNombre());

        this.imageView.setOnClickListener(this);
    }


    private void openWebsite(String item) {//Todo Sirve para abir el url cuando le damos a la imagen
        Uri webpage = Uri.parse(item);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(intent);

    }


    @Override
    public void onClick(View v) {//Todo Sirve para abir el url cuando le damos a la imagen
       openWebsite(nuevoItem.getUrl());
    }

    public boolean onOptionsItemSelected(@NonNull MenuItem item) {//Todo para poner la flecha del bar para dirigirse al home de la app
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}