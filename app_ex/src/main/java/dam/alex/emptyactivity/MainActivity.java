package dam.alex.emptyactivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

import dam.alex.emptyactivity.module.Item;

public class MainActivity extends AppCompatActivity implements MyAdapter.onItemClickListener,View.OnClickListener {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    Button add;
    Button deleteAll;
    Button restore;
    private ImageView imagenView;
    private ArrayList<Item> myDataset = new ArrayList<Item>();//TODO Cambiar array por arraylist

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        introducirInformacion();
        setUI();

    }

    private void setUI() {//TODO RecycleView  mostrar cada Item en un CardView
        this.add = findViewById(R.id.anyadir);
        this.deleteAll = findViewById(R.id.delete);
        this.restore = findViewById(R.id.restore);
        this.imagenView=findViewById(R.id.prohibido);
        this.imagenView.setVisibility(View.INVISIBLE);
        add.setOnClickListener(this);
        deleteAll.setOnClickListener(this);
        restore.setOnClickListener(this);

        recyclerView = findViewById(R.id.recyclerViewActivities);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MyAdapter(myDataset, this);
        recyclerView.setAdapter(mAdapter);



        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {//TODO para desplazar hacia la derecha y eliminarlo cuando se desplaza
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                myDataset.remove(viewHolder.getAdapterPosition());
                if (myDataset.size()==0){
                  imagenView.setVisibility(View.VISIBLE);
                }

                mAdapter.notifyDataSetChanged();
            }
        });

        itemTouchHelper.attachToRecyclerView(recyclerView);



    }

    private void introducirInformacion() {//TODO Crear las 8 versiones de android con toda la informacion
        Item lolipop = new Item(R.drawable.lolipop, "Lolipop", "Version: 5", "2014", "API: 21", "https://es.wikipedia.org/wiki/Android_Lollipop");
        myDataset.add(lolipop);
        Item marhmallow = new Item(R.drawable.marhmallow, "Marhmallow", "Version: 6", "2015", "API: 23", "https://es.wikipedia.org/wiki/Android_Marshmallow");
        myDataset.add(marhmallow);
        Item nougat = new Item(R.drawable.nougat, "Nougat", "Version: 7", "2016", "API: 24", "https://es.wikipedia.org/wiki/Android_Nougat");
        myDataset.add(nougat);
        Item oreo = new Item(R.drawable.oreo, "Oreo", "Version: 8", "2017", "API: 26", "https://es.wikipedia.org/wiki/Android_Oreo");
        myDataset.add(oreo);
        Item pie = new Item(R.drawable.pie, "Pie", "Version: 9", "2018", "API: 28", "https://es.wikipedia.org/wiki/Android_Pie");
        myDataset.add(pie);
        Item android10 = new Item(R.drawable.android10, "Android_10", "Version: 10", "2019", "API: 29", "https://es.wikipedia.org/wiki/Android_10");
        myDataset.add(android10);
        Item android11 = new Item(R.drawable.android11, "Android_11", "Version: 11", "2020", "API: 30", "https://es.wikipedia.org/wiki/Android_11");
        myDataset.add(android11);
        Item android12 = new Item(R.drawable.android12, "Android_12", "Version: 12", "2021", "API: 31", "https://es.wikipedia.org/wiki/Android_12");
        myDataset.add(android12);
    }

    @Override
    public void onItemClick(Item activiyName) {
        Bundle newBundel = new Bundle();
        newBundel.putSerializable("info", activiyName);
        startActivity(new Intent(this, ItemDetailActivity.class).putExtras(newBundel));

    }


    @SuppressLint({"NonConstantResourceId", "NotifyDataSetChanged"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.anyadir://TODO 3.1 Funcion de añadir nuevas versiones con datos fijos y posicionarse en el item añadido
                this.imagenView.setVisibility(View.INVISIBLE);
                Item tiramisu = new Item(R.drawable.tiramisu, "Tiramisu", "Version: 13", "2022", "API: 33", "https://www.xatakandroid.com/sistema-operativo/postre-secreto-android-13-seria-tiramisu");
                myDataset.add(tiramisu);
                this.imagenView.setVisibility(View.INVISIBLE);//TODO En el caso de que hayan Item en el array la imagen no se verá
                mAdapter.notifyDataSetChanged();
                layoutManager.scrollToPosition(myDataset.size()-1);
                break;
            case R.id.delete://TODO 3.2 Vaciar por completo la lista de Item

                myDataset.clear();
                this.imagenView.setVisibility(View.VISIBLE);//TODO En el caso de que no hayan Item en el array la imagen se verá
                mAdapter.notifyDataSetChanged();
                break;
            case R.id.restore://TODO 3.3 Restaurar la lista inicial de Items
                this.imagenView.setVisibility(View.INVISIBLE);//TODO En el caso de que hayan Item en el array la imagen no se verá
                myDataset.clear();
                introducirInformacion();
                mAdapter.notifyDataSetChanged();
                break;

        }


    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {//TODO 4.0 Restaura lo que se ha guardado anteriormente
        super.onRestoreInstanceState(savedInstanceState);
        this.myDataset.clear();
        this.myDataset.addAll((ArrayList<Item>) savedInstanceState.getSerializable("myDataset"));
        this.mAdapter.notifyDataSetChanged();

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {//TODO 4.0 Guardar los cambios en el onRestoreInstanceState
        super.onSaveInstanceState(outState);
        outState.putSerializable("myDataset",myDataset);



    }

}
