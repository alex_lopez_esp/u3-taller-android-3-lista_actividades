package dam.alex.emptyactivity.module;

import java.io.Serializable;

public class  Item implements Serializable {
    private int idImagen;
    private String nombre;
    private String version;
    private String anyolanzamiento;
    private String versionApi;
    private String url;

    public Item(int idImagen,String nombre, String version, String anyolanzamiento, String versionApi, String url) {
        this.idImagen = idImagen;
        this.nombre=nombre;
        this.version = version;
        this.anyolanzamiento = anyolanzamiento;
        this.versionApi = versionApi;
        this.url = url;
    }



    public int getIdImagen() {
        return idImagen;
    }

    public void setIdImagen(int idImagen) {
        this.idImagen = idImagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAnyolanzamiento() {
        return anyolanzamiento;
    }

    public void setAnyolanzamiento(String anyolanzamiento) {
        this.anyolanzamiento = anyolanzamiento;
    }

    public String getVersionApi() {
        return versionApi;
    }

    public void setVersionApi(String versionApi) {
        this.versionApi = versionApi;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
