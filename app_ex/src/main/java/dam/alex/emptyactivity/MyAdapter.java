package dam.alex.emptyactivity;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.alex.emptyactivity.module.Item;


public  class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    public interface onItemClickListener{
       void onItemClick(Item activiyName);

    }

    private ArrayList<Item> myDataSet;
   private  onItemClickListener listener;

    static  class MyViewHolder extends RecyclerView.ViewHolder{
        View view;
        ImageView imageView;
        TextView version;
        TextView api;
        TextView nombre;
        TextView fecha;




        public MyViewHolder(View view){
            super(view);
            this.view =view;
            this.imageView=view.findViewById(R.id.imagen);
            this.version= view.findViewById(R.id.version);
            this.api=view.findViewById(R.id.api);
            this.nombre =view.findViewById(R.id.nombre);
            this.fecha= view.findViewById(R.id.fecha);



        }
        public  void bind(Item activityName, onItemClickListener listener){
            this.view.setOnClickListener(v -> listener.onItemClick(activityName));


        }
    }

     MyAdapter(ArrayList<Item> myDataSet, onItemClickListener listener) {
        this.myDataSet=myDataSet;
        this.listener=listener;

    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View tv= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview,parent,false);

        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder,int position){
        viewHolder.bind(myDataSet.get(position),listener);

        viewHolder.imageView.setImageResource(myDataSet.get(position).getIdImagen());
        viewHolder.version.setText(myDataSet.get(position).getVersion());
        viewHolder.api.setText(myDataSet.get(position).getVersionApi());
        viewHolder.nombre.setText(myDataSet.get(position).getNombre());



    }

    @Override
    public int getItemCount(){
        return myDataSet.size();
    }






}





